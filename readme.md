# Six Frame Translator
###### Author: Tyr-Lys

## What is the Six Frame Translator?

The Six Frame Translator is tool which is able to find open reading frames (ORFs) in the six reading frames (-3, -2, -1, 1, 2, 3).
In the DNA sequences a single region can be specified in which the tool should look and find open reading frames.  
It does not support multifasta.  

![alt text](markDownImages/whole_screen.png)


## Quick start

Go to: http://localhost:8080/upload if started.  

Sample sequence: ATGTACTAAACTAGGGGCTTATAAAGCAATTTTCATGGGAACATCACGACATGATAGTATTCCGTCAAGTTGCATACATGTGTTCCCTGGTAAATTAGTGTTG

Fill in a sequence in the left text area, select a reading frame and click on "Upload your sequence".  
  
If not understood please click the tab video demonstration in the the bottom right corner and watch the video on fullscreen.  

![alt text](markDownImages/quick_start.png)

## Requirements:

[Tomcat 8.5.3 server](https://tomcat.apache.org/download-80.cgi#8.5.30)  
[Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)  

## Tutorial

Go to: http://localhost:8080/upload if started.  

1. Enter your DNA sequence or Fasta sequence in the text area in the top left.(It does not support MultiFasta.)  

2. Select the reading frames in which you wish to find open reading frames. 
(If no reading frame is selected it picks the default value of reading frame 1.)

3. (Optional) Select a start value, this is the position where the slice of the DNA sequence starts.  
    (When left empty a 0 will be filled in and the start of the sequence is used.)
    (e.g. Start value = 30, then a sequence  with the length of 100 becomes the region of 30-100 is used to find open reading frames.) **  

4. (Optional) Select a stop value, this is where the slice ends.
    (When left empty the length of the sequence will be filled in and the end of the sequence is used.)
    (e.g. stop value = 70, then a sequence with the length of 100 will be cut into a sequence 0-70.) **
    
5. (Optional) Select a minimum value of which open reading frames at least should be.

6. If all (correctly) filled in the button Submit your sequence can be pushed and the found open reading frames will appear in the bottom left text area.
    
** If a start value of 30 and a stop value of 70 are used with a sequence which has 100 base pairs it finds open reading frames in the region 30-70. 

## Rules on the input fields and outcome.

Allowed in the top left text area for filling in sequences:  
* A single Fasta header (\>) which must be separated with an enter from the DNA sequence.
* Nucleotides upper and lower case.
* White spaces and tabs after a new line. (These are removed during the process).  
  
Not allowed in the top left text area:
* Numbers
* Any other character except the 4 nucleotide characters.
* White spaces and tabs within the DNA sequence.
* It does not support multifasta. 

Minimum length, start and stop affect the Upload your sequence button, if a letter are filled in the text boxes it disables the button.
![alt text](markDownImages/nonsense_letters.png)   
When there are values below 0 in the fields of minimum length, start or stop it will disable the button.  
![alt text](markDownImages/below_zero.png)
When the minimum length , start or stop value are larger than the DNA sequence it will disable the button.

![alt text](markDownImages/minimum_larger_than_input.png)
Larger  

![alt text](markDownImages/still_works_same_length.png)
Smaller

When te start value is larger than the stop value -3 nucleotides the button will be disabled.
![alt text](markDownImages/start_bigger_stop.png)
  
When JavaScript fails to deny the user an upload, it is caught in the Java backend and will most likely revert to the default settings(reading frame = 1, minimum length = 0, start = 0 stop = maximum length), depending on the given input.

## Header explanation:  
  
With the generated headers you can easily separate results acquired by the tool.  

Reading frame number: (-1,-2,-3,1,2,3)  
Number of found orf in reading frame: (0,1,2,3,4,5 etc.)  
Start and stop position Forward: (60-96)  
Original header: (> E.Coli 123456)  

Header example:  
\> -3.1.30-60.  

Reading frame: -3  
Numbered ORF in reading frame: 1  
Start of reading frame position (ATG) : 30  
Stop of ORF found at: 60  
No original header was given.

Header example without Fasta header:  
\> -3.1.30-60.  

Reading frame: -3  
Numbered ORF in reading frame: 1  
Start of reading frame position (ATG) at : 30  
Stop of ORF or end of Sequence at: 60  
Original header:

Header example with Fasta header:  
\> -2.2.42-99999>.> Slartibartfast 
Reading frame: -2  
Numbered ORF in reading frame: 2  
Start of reading frame position (ATG) at : 42  
Stop of ORF or end of Sequence at: 99999
\> Broken value, did not find a stop codon.
Original header: > Slartibartfast


## Results from sample sequences:

Given the following sequence: 
ATGTACTAAACTAGGGGCTTATAAAGCAATTTTCATGGGAACATCACGACATGATAGTATTCCGTCAAGTTGCATACATGTGTTCCCTGGTAAATTAGTGTTG

With these parameters:  
  
reading frames: -3,-2,-1,1,2,3  
minimum length: 0  
start = 0  
stop = maximum length  

Result:

    > -3.1.44-33.  
    ____________DNA Sequence___________   
    ATGTTCCCATGA  
    ------------DNA Translation------------  
    MFP*  
      
      
    > -2.1.75-19.  
    ____________DNA Sequence___________   
    ATGCAACTTGACGGAATACTATCATGTCGTGATGTTCCCATGAAAATTGCTTTATAA  
    ------------DNA Translation------------  
    MQLDGILSCRDVPMKIAL*  
      
      
    > -1.1.79-65.  
    ____________DNA Sequence___________    
    ATGTATGCAACTTGA  
    ------------DNA Translation------------  
    MYAT*  
      
      
    > -1.2.52-44.  
    ____________DNA Sequence___________   
    ATGTCGTGA  
    ------------DNA Translation------------  
    MS*  
      
      
    > 1.1.1-9.  
    ____________DNA Sequence___________  
    ATGTACTAA  
    ------------DNA Translation------------  
    MY*  
      
      
    > 2.1.35-103>.  
    ____________DNA Sequence____________  
    ATGGGAACATCACGACATGATAGTATTCCGTCAAGTTGCATACATGTGTTCCCTGGTAAATTAGTGTTG  
    ------------DNA Translation------------  
    MGTSRHDSIPSSCIHVFPGKLVL  
      
      
    > 3.1.51-98.  
    ____________DNA Sequence____________  
    ATGATAGTATTCCGTCAAGTTGCATACATGTGTTCCCTGGTAAATTAG  
    ------------DNA Translation------------  
    MIVFRQVAYMCSLVN*  


Given the following Fasta:  

\>NC_001133.9 Saccharomyces cerevisiae S288c chromosome I, complete sequence
ccacaccacacccacacacccacacaccacaccacacaccacaccacacccacacacacacatCCTAACACTACCCTAAC
ACAGCCCTAATCTAACCCTGGCCAACCTGTCTCTCAACTTACCCTCCATTACCCTGCCTCCACTCGTTACCCTGTCCCAT
TCAACCATACCACTCCGAACCACCATCCATCCCTCTACTTACTACCACTCACCCACCGTTACCCTCCAATTACCCATATC
CAACCCACTGCCACTTACCCTACCATTACCCTACCATCCACCATGACCTACTCACCATACTGTTCTTCTACCCACCATAT


With these parameters:  
  
reading frames: -2  
minimum length: 30  
start = 0  
stop = maximum length


    > -2.1.298-266.>NC_001133.9 Saccharomyces cerevisiae S288c chromosome I, complete sequence  
    ____________DNA Sequence____________  
    ATGGTGAGTAGGTCATGGTGGATGGTAGGGTAA  
    ------------DNA Translation------------  
    MVSRSWWMVG*  
      
      
    > -2.2.190-161.>NC_001133.9 Saccharomyces cerevisiae S288c chromosome I, complete sequence  
    ____________DNA Sequence____________  
    ATGGATGGTGGTTCGGAGTGGTATGGTTGA  
    ------------DNA Translation------------  
    MDGGSEWYG*  
      
      
    > -2.3.160-119.>NC_001133.9 Saccharomyces cerevisiae S288c chromosome I, complete sequence  
    ____________DNA Sequence____________  
    ATGGGACAGGGTAACGAGTGGAGGCAGGGTAATGGAGGGTAA  
    ------------DNA Translation------------  
    MGQGNEWRQGNGG*  



