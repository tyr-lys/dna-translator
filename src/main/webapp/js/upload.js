
// Get characters in the textarea where the sequence is filled in.
var inputArea = document.getElementById("areacss");

inputArea.addEventListener("input", function(){
});

// Get characters in text box for the optional minimum length.
var minimumLength = document.getElementById("minlen");

minimumLength.addEventListener("input",function(){
});

// Get characters in text box for the optional start position.
var startValue = document.getElementById("ostart");

startValue.addEventListener("input", function(){
});

// Get characters in the text box for the optional end position.
var stopValue = document.getElementById("oend");

stopValue.addEventListener("input", function(){
});

// Validates
function validateInputSubmissionButton(){
    var buttonStatus = document.getElementById("submissionButton");
    console.log(parseInt(startValue.value),parseInt(stopValue.value),parseInt(minimumLength.value),parseInt(inputArea.value.length));
    if (
        isNaN(startValue.value)||
        isNaN(stopValue.value)||
        isNaN(minimumLength.value)||
        parseInt(startValue.value) < 0 ||
        parseInt(stopValue.value) < 0 || // Below 0.
        parseInt(minimumLength.value) < 0 ||
        parseInt(inputArea.value.length) < 0 ||
        (parseInt(stopValue.value) -3) <= parseInt(startValue.value) || // Start value larger than stop value or less than 3 nucleotides.
        parseInt(startValue.value) > parseInt(inputArea.value.length) || // Start value larger than input.
        parseInt(stopValue.value) > parseInt(inputArea.value.length) || // Stop value larger than input.
        parseInt(minimumLength.value) > parseInt(inputArea.value.length) || // Minimum length larger than input.
        parseInt(stopValue.value) - parseInt(startValue.value) < parseInt(minimumLength.value)
    )
    {
        //Disable button.
        buttonStatus.disabled = true;
    }else {
        //Enable button.
        buttonStatus.disabled = false;
    }
}

// Starts at the beginning of the script and checks for keystrokes and focus blur of input fields.
window.onload = function(){
    var inputs = document.getElementsByTagName('input');
    for(var i=0; i<inputs.length; i++){
        if(inputs[i].type === "text" || inputs[i].type === "textarea"){
            inputs[i].onkeyup = validateInputSubmissionButton;
            inputs[i].onblur = validateInputSubmissionButton;
            inputs[i].onfocus = validateInputSubmissionButton;
        }
    }
};
