<%--
  Created by IntelliJ IDEA.
  User: Nimda
  Date: 12/16/17
  Time: 3:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<link href="css/uploadpage.css" rel="stylesheet" type="text/css">
<head>
    <title>Six Frame Translator</title>
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="bs-example container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h5 id="translatortitle">Six Frame Translator</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
    <form method="post" name="inputarea">
<textarea id="areacss" name="inputDataArea" placeholder="Fill here your sequence with or without fasta header.">
</textarea>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <h6 class="h5">Reading frames</h6>
                <div class="form-row">
                    <div class="form-col">
                        <div class="form-check">
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="1">
                                <label class="form-check-label">1</label>
                            </div>
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="-1">
                                <label class="form-check-label">-1</label>
                            </div>
                        </div>
                        <div class="form-check">
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="2">
                                <label class="form-check-label">2</label>
                            </div>
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="-2">
                                <label class="form-check-label">-2</label>
                            </div>
                        </div>
                        <div class="form-check">
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="3">
                                <label class="form-check-label">3</label>
                            </div>
                            <div class="form-check-inline col-sm-1">
                                <input type="checkbox" class="form-check-input" name="frameShiftBox" value="-3">
                                <label class="form-check-label">-3</label>
                            </div>
                        </div>

                        <%-- Minimum ORF length, Start and Stop position--%>
                    </div>
                    <div class="form-col">

                        <label>Minimum Length</label>
                        <input type="text" class="form-control" name="optionalMinimumLength" id="minlen">

                    </div>
                    <div class="form-col">

                        <label>Start</label>
                        <input type="text" class="form-control" name="optionalStart" id="ostart">

                    </div>
                    <div class="form-col">

                        <label>Stop</label>
                        <input type="text" class="form-control" name="optionalEnd" id ="oend">

                    </div>
                </div>
        <input type="submit" value="Upload your Sequence"  class="btn btn-primary" id="submissionButton">
        </div>
    </form>
    </div>
</div>



<div class = "row">
    <div class="col-sm-6">
<textarea id="result" name="resultDataArea" placeholder="The results of the DNA sequence will be displayed here."><c:forEach var="orf" items="${requestScope.resultArea}">${orf}
</c:forEach>
</textarea>
    </div>
    <div class="col-sm-6">
        <%-- Tab bar --%>
        <ul class="nav nav-tabs justify-content-center" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#Textual" role="tab" data-toggle="tab">Textual explanation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#Videm" role="tab" data-toggle="tab">Video demonstration</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade show active" id="Textual">
                <p class="infotext">
                    This tool is able to find open reading frames(ORFs) in a DNA sequence or fasta sequence.
                    It is possible to specify the minimum length of which an ORFs should be, and a start and stop position in a sequence.
                </p>
                <p class="infotext">
                    Step 1:
                    In the upper left text area, fill in a sequence.
                </p>
                <p class="infotext">
                    Step 2:
                    Select a reading frame which should be used. (1 Is the standard if none is selected.)
                </p>
                <p class="infotext">
                    Step 3:(Optional)
                    Fill in the start and stop position.(This is the part which is processed.)
                </p>
                <p class="infotext">
                    Step 4:(Optional)
                    Fill in the minimum ORF length. (DNA sequence length, so three times the length of the amino acid.)
                </p>
                <p class="infotext">
                    Step 5:
                    Press the upload sequence button to generate results.
                </p>
                <p class="infotext">
                    Step 6:
                    Copy the text in the bottom left text area.
                </p>
                <p class="infotext">
                    It does NOT support multifasta.
                    For extra information visit the <a href="https://bitbucket.org/tyr-lys/dna-translator">tutorial</a>.
                </p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="Videm">
                <video width="360" src="videos/demo.m4v"
                type="video/mp4" controls>
                <p>
                Your browser does not support HTML5 video.
                </p>
                </video>
            </div>
        </div>
</div>




<script src="js/upload.js"></script>
<!-- jQuery library -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>

<!-- Popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
</body>
</html>
