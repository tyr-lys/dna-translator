package models;

/**
 * ORF product which is a DNA sequence with a header, DNA and amino chain/sequence.
 */
public class OrfProduct {
    // New ORF header.
    private String orfHeader;
    // DNA ORF sequence.
    private String openReadingFrameTripletString;
    // Amino ORF sequence.
    private String openReadingFrameAminoAcidArrayString;

    /**
     * Gives back the header.
     * @return header.
     */
    public String getOrfHeader() {
        return orfHeader;
    }

    /**
     * Gives back the DNA sequence.
     * @return DNA sequence.
     */
    public String getOpenReadingFrameTripletString() {
        return openReadingFrameTripletString;
    }

    /**
     * Gives back a peptide chain/sequence.
     * @return Peptide chain/sequence.
     */
    public String getOpenReadingFrameAminoAcidArrayString() {
        return openReadingFrameAminoAcidArrayString;
    }

    /**
     * The ORF constructor.
     * @param orfHeader ORFheader.
     * @param openReadingFrameTripletString DNA sequence.
     * @param openReadingFrameAminoAcidString Peptide chain/sequence.
     */
    OrfProduct(String orfHeader, String openReadingFrameTripletString,
               String openReadingFrameAminoAcidString){
        this.orfHeader = orfHeader;
        this.openReadingFrameTripletString = openReadingFrameTripletString;
        this.openReadingFrameAminoAcidArrayString = openReadingFrameAminoAcidString;
    }

    /**
     * String representation of the ORF product.
     * @return String representation of ORF product.
     */
    @Override
    public String toString(){
        return orfHeader + "\n" +
                openReadingFrameTripletString + "\n" +
                openReadingFrameAminoAcidArrayString;
    }
}
