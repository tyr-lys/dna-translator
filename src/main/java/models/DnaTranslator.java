package models;

public class DnaTranslator {

    /**
     * Converts DNA triplets to their corresponding amino acids.
     * @param codon A triplet of nucleotides.
     * @return Gives back the amino acid that corresponds with the DNA triplet.
     */
    public String translateDna(String codon){
        String aminoAcid;
     switch(codon){
         case "TTT":case "TTC":
             aminoAcid="F";
             break;
         case "TTA":case "TTG":case "CTT":case"CTC":case"CTA":case "CTG":
             aminoAcid="L";
             break;
         case "ATT":case "ATC":case "ATA":
             aminoAcid="I";
             break;
         case "ATG":
             aminoAcid="M";
             break;
         case "GTT":case "GTC":case "GTA":case "GTG":
             aminoAcid="V";
             break;
         case "TCT":case "TCC":case "TCA":case "TCG":
             aminoAcid="S";
             break;
         case "CCA":case "CCC":case "CCG":case "CCT":
             aminoAcid="P";
             break;
         case "ACT":case "ACC":case "ACA":case "ACG":
             aminoAcid="T";
             break;
         case "GCT":case "GCC":case "GCA":case "GCG":
            aminoAcid="A";
            break;
         case "TAT":case "TAC":
             aminoAcid="Y";
             break;
         case "TAA":case "TAG":case "TGA":
             aminoAcid="*";
             break;
         case "CAT":case "CAC":
             aminoAcid="H";
             break;
         case "CAA":case "CAG":
             aminoAcid="Q";
             break;
         case "AAT":case "AAC":
             aminoAcid="N";
             break;
         case "AAA":case "AAG":
             aminoAcid="K";
             break;
         case "GAT":case "GAC":
             aminoAcid="D";
             break;
         case "GAA":case "GAG":
             aminoAcid="E";
             break;
         case "TGT":case "TGC":
             aminoAcid="C";
             break;
         case "TGG":
             aminoAcid="W";
             break;
         case "CGT":case "CGC":case "CGA":case "CGG": case "AGA":case "AGG":
             aminoAcid="R";
             break;
         case "AGT":case "AGC":
             aminoAcid="S";
             break;
         case "GGT":case "GGC":case "GGA":case "GGG":
             aminoAcid="G";
             break;
             default:
                 throw new IllegalArgumentException("Invalid nucleotides where used.");
     }
        return aminoAcid;
    }
}
