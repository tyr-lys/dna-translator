package models;

import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Math.abs;

public class DnaParser {
    // DNA sequence of an ORF.
    private ArrayList<String> tempDna = new ArrayList<String>();
    // Amino sequence of an ORF.
    private ArrayList<String> tempAmino = new ArrayList<String>();
    // All ORFs of a single readingFrame combined.
    private ArrayList<OrfProduct> ORFsInReadingFrame = new ArrayList<>();
    // Number of found ORFs in a single reading frame. (Iterative)
    private int foundORFsPerReadingFrame = 0;
    // Reading frame value itself.
    private int readingFrame;
    // Whole DNA sequence, without header if it is available.
    private String inputDNASequence;
    // Header if available.
    private String originalHeader;
    // Optional value of which lengths the ORFs should be.
    private int minimumLength;
    // Start of the ORF.
    private int startORF;
    // Stop of the ORF.
    private int stopORF;


    /**
     * DNA parser object which is used to manage the data going in and out to form an ORFProduct.
     * @param readingFrame Reading frame number.
     * @param inputDNASequence Whole DNA sequence, without header.
     * @param originalHeader Header which came from the original fasta sequence.
     * @param minimumLength The length of which an ORF at least should be.
     */
    public DnaParser(int readingFrame, String inputDNASequence,String originalHeader, int minimumLength){
        this.readingFrame = readingFrame;
        this.inputDNASequence = inputDNASequence;
        this.originalHeader = originalHeader;
        this.minimumLength = minimumLength;
        parseDNA(readingFrame,inputDNASequence);
    }

    /**
     * Iterates over the DNA sequence with its reading frame.
     * @param readingFrame Reading frame in which it looks.
     * @param inputDnaSequence DNA sequence.
     */
    private void parseDNA(int readingFrame,String inputDnaSequence){
        // Start position.
        String dnaTriplet;
        String aminoAcid;
        boolean inOpenReadingFrame=false;
        // Call DNA translator
        DnaTranslator dnaTranslation = new DnaTranslator();
        // Calculate correct position for computer reading not for human reading.
        int correctStartingPosition = calculateCorrectCodonPositionsForReadingFrame(readingFrame);

        // Iterate with steps of 3 and determine what it is and make decision based on that.
        for (int currentCodonPositon = correctStartingPosition; currentCodonPositon <= inputDnaSequence.length(); currentCodonPositon+=3){
            try {
                // DNA triplet and its translation.
                dnaTriplet = inputDnaSequence.substring(currentCodonPositon, currentCodonPositon + 3);
                aminoAcid = dnaTranslation.translateDna(dnaTriplet);
                // In case it exactly ends on the last segment of the sequence this is initiated, else it generates and IndexOutOfBoundsException.
                if (currentCodonPositon == inputDnaSequence.length() - 3) {

                    finalStepProcedure(dnaTriplet, aminoAcid, inOpenReadingFrame, currentCodonPositon);
                } else {
                    inOpenReadingFrame = buildDNAProteinORF(dnaTriplet, aminoAcid, inOpenReadingFrame, currentCodonPositon);
                }
            }
            // Final step if not a triplet remains but not the exact ending.
            catch(IndexOutOfBoundsException IOOBE){
                stopORF = inputDNASequence.length()-3;
                createORFObject(minimumLength,true);
            }
        }
    }

    /**
     * Builds both the amino and the DNA sequence at the same time. If it is in the correct state it is added.
     * @param dnaTriplet Tri nucleotides.
     * @param amino Translated DNA triplet.
     * @param inOpenReadingFrame Describes if the parser is currently in the open reading frame.
     * @param readingPosition Position at which parser is.
     * @return The boolean result determines which step should be taken next.
     */
    private boolean buildDNAProteinORF (String dnaTriplet,String amino, boolean inOpenReadingFrame,int readingPosition){
        switch (amino) {
            // In case of start codon.
            case "M":
                // Already inside an ORF.
                if (inOpenReadingFrame) {
                    addToOrf(dnaTriplet, amino);
                // New ORF.
                } else {
                    inOpenReadingFrame = true;
                    startORF = readingPosition;
                    addToOrf(dnaTriplet, amino);
                }
                break;
            // In case of a stop codon.
            case "*":
                if (inOpenReadingFrame) {
                    inOpenReadingFrame = false;
                    stopORF = readingPosition;
                    addToOrf(dnaTriplet, amino);
                    createORFObject(minimumLength, false);
                }
                break;
            // All other non stop or start codons.
            default:
                if (inOpenReadingFrame) {
                    addToOrf(dnaTriplet, amino);
                }
                break;
        }
        return inOpenReadingFrame;
    }

    /**
     * Only executed if the readingFrame exactly ends on the triplet, has several other choices as buildDNAProteinORF.
     * @param dnaTriplet Tri nucleotides.
     * @param amino Translated DNA triplet.
     * @param inReadingFrame Describes if the parser is currently in the open reading frame.
     * @param readingPosition Position at which parser is.
     */
    private void finalStepProcedure(String dnaTriplet,String amino, boolean inReadingFrame, int readingPosition){
        // In the final step it does not matter which amino acid is given, it only matters if it was in an open reading frame.
        if(inReadingFrame){
            // Normal termination signal.
            if(amino.equals("*")) {
                addToOrf(dnaTriplet, amino);
                stopORF = inputDNASequence.length() - 3;
                createORFObject(minimumLength,false);
            } else{
            // Broken ORF.
                addToOrf(dnaTriplet, amino);
                stopORF = inputDNASequence.length() - 3;
                createORFObject(minimumLength,true);
            }
        }else{
            // Only add it in case it Methionine, the rest of the codons do not start an ORF.
            if(amino.equals("M")) {
                addToOrf(dnaTriplet, amino);
                startORF = readingPosition;
                stopORF = inputDNASequence.length() - 3;
                createORFObject(minimumLength, true);
            }
        }
    }


    /**
     * Makes an ORFproduct with all its properties out of the info acquired during the execution of parseDNA.
     * @param minimumLength Minimum length of which an ORF should be.
     * @param broken Result of the ORF, it did not receive a stop codon at the end but the sequence stopped.
     */
    private void createORFObject (int minimumLength, boolean broken){
        // Make one DNA sequence (String) out of all loose fragments.
        String foundOrf = String.join("",tempDna);
        CreateHeader headMaker = new CreateHeader();
        if (foundOrf.length() >= minimumLength){
            foundORFsPerReadingFrame += 1;
            recalculateStartStopOnValues(readingFrame);
            ORFsInReadingFrame.add(
                    new OrfProduct(headMaker.createNewHeaderOfData(
                        readingFrame,
                        foundORFsPerReadingFrame,
                        startORF, // Point of finding ATG start codon in ORF.
                        stopORF, // Point of finding stop codon in ORF.
                        originalHeader, // Header acquired from fasta file
                        broken), // Test if it ended with a stop codon.
                        foundOrf,
                        String.join("",tempAmino))); // Combine amino acid fragments.
        }
        // Clear the DNA and Amino sequence ArrayList.
        cleanUpRemainders();
    }

    /**
     * Calculate the correct values for the new header.
     * @param originalReadingFrameValue The value for the original reading frame.
     */
    private void recalculateStartStopOnValues (int originalReadingFrameValue){
        int tmpstop;
        int tmpstart;
        if (originalReadingFrameValue < 0) {
            tmpstop = stopORF+2;
            tmpstart = startORF;
            stopORF = inputDNASequence.length() - tmpstart;
            startORF = inputDNASequence.length() - tmpstop;
            if (startORF < 1) {
                startORF = 1;
            }
        } else{
        stopORF += 3;
        startORF += 1;
        }
    }


    /**
     * Remove the existing DNA and amino sequences.
     */
    private void cleanUpRemainders(){
        tempAmino.clear();
        tempDna.clear();
    }

    /**
     * Add the triplet segments and amino acids.
     * @param dnaTriplet Tri nucleotides.
     * @param aminoAcid Translation from DNA triplet.
     */
    private void addToOrf(String dnaTriplet, String aminoAcid){
        tempDna.add(dnaTriplet);
        tempAmino.add(aminoAcid);
    }
    private int calculateCorrectCodonPositionsForReadingFrame(int modifiableReadingFrame){
        if (modifiableReadingFrame < 0){
            modifiableReadingFrame = abs(modifiableReadingFrame)-1;
        } else if(modifiableReadingFrame == 0){
            // Just do nothing when it is zero.
        }else{
            modifiableReadingFrame = modifiableReadingFrame-1;
        }
        return modifiableReadingFrame;
    }

    /**
     * @return Gives back if the parser is in the reading frame.F
     */
    public ArrayList<OrfProduct> getORFsInReadingFrame() {
        return ORFsInReadingFrame;
    }

}
