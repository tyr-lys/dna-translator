package models;

/**
 * Simple class with one method that converts characters.
 */
public class ComplementConverter {

    /**
     * Makes the complement DNA character of a String.
     * @param dnaCharacter Is a single DNA nucleotide in String form.
     * @return Gives back the complement character.
     */
    public String convertDnaCharacter(String dnaCharacter){
        switch (dnaCharacter){
            case "A":
                dnaCharacter = "T";
                break;
            case "C":
                dnaCharacter = "G";
                break;
            case "G":
                dnaCharacter = "C";
                break;
            case "T":
                dnaCharacter = "A";
                break;
                default:
                 throw new IllegalArgumentException("An invalid character is used???");
        }
    return dnaCharacter;
    }
}
