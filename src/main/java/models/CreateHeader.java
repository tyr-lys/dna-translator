package models;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class which forms the header of the available info.
 */
public class CreateHeader {
    /**
     * * Creates a header for the produced orf.
     * Consisting of:
     * Reading frame number: (-1,-2,-3,1,2,3)
     * Number of found orf in reading frame: (0,1,2,3,4,5 etc.)
     * Start and stop position Forward: (60-96)
     * Original header: (&#062; E.Coli 123456)
     * @param dnaReadingFrame int value between -3 and 3.
     * @param foundORFsInReadingFrame Iterative number of found ORFS.
     * @param startPosition Starting position of the ORF in modified and unmodified sequences.
     * @param stopPosition Ending position of the ORF in modified and unmodified sequences.
     * @param originalHeader Old header if available.
     * @param broken In case the reading frame goes on but the sequence ends.
     * @return New header formed of the given components.
     */
    public String  createNewHeaderOfData(int dnaReadingFrame,int foundORFsInReadingFrame,
                                         int startPosition,int stopPosition,String originalHeader,boolean broken){
        // Test if there was a header which can be added.
        originalHeader = originalHeader != null ? "."+originalHeader : "";
        // Insert the strings into an arrayList.
        // Some tests to determine which characters to be inserted in which order, all private methods.
        String brokenCharacterValue = acquireBrokenValueCharacter(broken);
        String correctPositionOfReadingFrameInSequence = startStopOrderDeterminator(dnaReadingFrame,startPosition,stopPosition);

        String [] finishedStringArray = new String []
                // Start of new header.
                {"> ",
                        // Reading frame value.
                        String.valueOf(dnaReadingFrame)
                        ,".",
                        // Number ORF in readingFrame.
                        String.valueOf(foundORFsInReadingFrame),
                        ".",
                        correctPositionOfReadingFrameInSequence,
                        brokenCharacterValue,
                        // If a header was in the text it is represented at the end of the new header.
                        originalHeader};
        // Return the array as a String.
        return String.join("",finishedStringArray);
    }


    /**
     * Returns the character based on boolean, if the length was to long it returns &#062;,
     * otherwise it returns an empty string.
     * @param brokenValue Boolean that determines result.
     * @return Value that represent broken or not broken.
     */
        private String acquireBrokenValueCharacter(boolean brokenValue){
            if (brokenValue){
                return ">";
            }
            else{
                return "";
            }

        }

    /**
     * Based on the readingFrame it switches the position of the start and stop.
     * @param dnaReadingFrame Reading frame number.
     * @param startPosition Start position of the ORF.
     * @param stopPosition Stop position of the ORF.
     * @return Concatenation of components in the correct order depending on the reading frame.
     */
        private String startStopOrderDeterminator(int dnaReadingFrame ,int startPosition, int stopPosition) {
            if (dnaReadingFrame >= 0) {
                // Positive
                return startPosition + "-" + stopPosition;
            } else {
                // Negative
                return stopPosition + "-" + startPosition;
            }
        }

    }

