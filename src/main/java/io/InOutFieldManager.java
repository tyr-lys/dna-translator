package io;

import models.OrfProduct;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class has several methods which are used to convert the input into the correct format.
 */
public class InOutFieldManager {

    /**
     * Tests whether a DNA sequence has valid characters (ACGT) if not it returns the inputSequence.
     * @param inputSequence Is the whole DNA sequence.
     * @return String resultSequence XOR inputSequence
     */
    public String validateInputSequence(String inputSequence) {
        String resultSequence;
        // Match if any not ACGT characters are found.
        Pattern dnaPattern = Pattern.compile("([^ACGTacgt]+)");
        Matcher dnaMatcher = dnaPattern.matcher(inputSequence);
        if (dnaMatcher.find()) {
            resultSequence = dnaMatcher.group();
        } else {
            resultSequence = inputSequence;
        }
        return resultSequence;
    }


    /**
     * Reads the ArrayList of OrfProducts which is then converted to Strings which can be given to the text area on the
     * webpage.
     * @param arrayListOfSequenceProducts All generated ORF products.
     * @return orfResultsInStringForm
     */
    public static ArrayList<String> unpackSequenceProducts(ArrayList<OrfProduct> arrayListOfSequenceProducts) {
        ArrayList<String> orfResultsInStringForm = new ArrayList<String>();
        for (OrfProduct arrayListOfSequenceProduct : arrayListOfSequenceProducts) {
            // Add The header from the Arraylist.
            orfResultsInStringForm.add(arrayListOfSequenceProduct.getOrfHeader());
            // Simple separation line added after the header for the DNA sequence.
            orfResultsInStringForm.add("____________DNA Sequence____________");
            // Manage lengths of DNA sequence.
            orfResultsInStringForm.addAll(sequenceLengthManager(arrayListOfSequenceProduct.
                    getOpenReadingFrameTripletString()));
            // Simple seperation line added after the DNA sequence for the Translation.
            orfResultsInStringForm.add("------------DNA Translation------------");
            // Manage lengths of amino sequence.
            orfResultsInStringForm.addAll(sequenceLengthManager(arrayListOfSequenceProduct.
                    getOpenReadingFrameAminoAcidArrayString()));
            // After completion of an ORF add a newline character.
            orfResultsInStringForm.add("\n");
        }
        return orfResultsInStringForm;
    }

    /**
     * Manages the lengths of DNA and amino sequences to a maximum of 80 characters as output.
     * @param sequenceProduct String of the sequence.
     * @return A String cut up into pieces with the length of 80 characters.
     */
    private static ArrayList<String> sequenceLengthManager(String sequenceProduct) {
        ArrayList<String> sequenceFragments = new ArrayList<String>();
        // Cut string into pieces if it is larger than 80 characters long.
        if (sequenceProduct.length() > 80) {
            for (int i = 0; sequenceProduct.length() > i; i += 80) {
                // Within the limit of the String.
                try {
                    sequenceFragments.add(sequenceProduct.substring(i, i + 80));
                // Step size to large just add the rest.
                } catch (IndexOutOfBoundsException IOOBE) {
                    sequenceFragments.add(sequenceProduct.substring(i));
                }
            }
        } else {
            // It is shorter than 80 characters, just add it.
            sequenceFragments.add(sequenceProduct);
        }
        return sequenceFragments;
    }

    /**
     * Tests the given sequence if a fasta header is available by looking at the first fragment for a &#062;, it
     * does not support multifasta.
     * @param inputSequence Whole sequence including potential fasta header.
     * @return Split sequence with at the first position a the sequence, second position the potential header.
     */
    public static String[] findFastaHeaderMakeSequence(String inputSequence) {
        // Split input on newline characters.
        String[] inputSequenceArray = inputSequence.trim().split("\r\n");
        // Remove any trailing whitespace in any element of the sequence.
        inputSequenceArray = trimStringArray(inputSequenceArray);
        // The header and the sequence will be stored in a String array.
        String[] determinedFormatSequenceArray = new String[2];
        // Exclude first element, which is the header.
        if (inputSequenceArray[0].startsWith(">")) {
            // Sequence
            determinedFormatSequenceArray[0] = String.join("",
                    Arrays.copyOfRange(inputSequenceArray, 1, inputSequenceArray.length)).toUpperCase();
            // Header
            determinedFormatSequenceArray[1] = inputSequenceArray[0];
            return determinedFormatSequenceArray;
        // Include first element, there is no header available.
        } else {
            determinedFormatSequenceArray[0] = String.join("", inputSequenceArray).toUpperCase();
            determinedFormatSequenceArray[1] = "";
            return determinedFormatSequenceArray;
        }
    }

    /**
     * Remove the trailing whitespaces from each element in the given String Array.
     * @param inputArray String array.
     * @return String array with trailing whitespaces removed.
     */
    private static String[] trimStringArray(String[] inputArray) {
        String[] outputArray = new String[inputArray.length];
        for (int item = 0; inputArray.length > item; item++) {
            outputArray[item] = inputArray[item].trim();
        }
        return outputArray;
    }

    /**
     * Gets data from a field and tests if it contains numbers or nonsense. Nonsense is converted to any empty String.
     * @param inputName Attribute on the website.
     * @param request request object from website.
     * @return Recovered and reshaped attribute into null if it is no digit.
     */
    public static String testInputIfFilledIn(String inputName, HttpServletRequest request) {
        if (request.getParameter(inputName) != null) {
            // Remove all characters that are not part of the numbers but are filled in.
            String tempString = request.getParameter(inputName)
                    .replace("\n", "")
                    .replace("\r", "")
                    .replace("\t", "");
            // Test for non digit characters.
            Pattern nonDigitCharacter = Pattern.compile("[^\\d]+");
            Matcher nonDigitTest = nonDigitCharacter.matcher(tempString);
            if (nonDigitTest.find()) {
                return "";
            } else {
                return tempString;
            }
        } else {
            return null;
        }
    }

    /**
     * In case no values is given for the frameShift a value of 3 will be given.
     * @param frameShift frameShift value.
     * @return If none is given it returns a frameShift of 3 otherwise its given frameShift.
     */
    public static String[] testFrameShiftInput(String[] frameShift) {
        if (frameShift == null) {
            return new String[]{"1"};
        } else {
            return frameShift;
        }
    }
}
