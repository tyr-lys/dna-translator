package io;

import java.util.Arrays;

/**
 * With this class reading frames can be converted into a different data type.
 */
public class FrameConverter {
    /**
     * The method converts reading frames that are in string type to reading frames in integer type
     * it results in a sorted integer array.
     * @param stringReadingFrames String array of readingFrame numbers.
     * @return Int array with reading frame values.
     */
    public int [] stringFramesToIntegerFrames(String [] stringReadingFrames){
        // Make a new array with the same length as the string array given.
        int [] intReadingFrames = new int [stringReadingFrames.length];
        // Convert the strings into ints in which order they came.
        for(int i =0; i < stringReadingFrames.length;i++){
            intReadingFrames[i] = Integer.parseInt(stringReadingFrames[i]);
        }
        // Just in case something went wrong with the ordering.
        Arrays.sort(intReadingFrames);
        return intReadingFrames;
    }

}
