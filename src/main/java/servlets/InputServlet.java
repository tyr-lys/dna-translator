package servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import io.InOutFieldManager;
import models.OrfProduct;
import program.RunningProgram;


@WebServlet(name = "InputServlet", urlPatterns = "/upload")
public class InputServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get the sequence or fasta file.
        String inputFileOrSequence = request.getParameter("inputDataArea");

        // Get frame shift values.
        String[] requestedSequenceFrameShifts = io.InOutFieldManager.testFrameShiftInput(request.getParameterValues("frameShiftBox"));

        // Get header of the input.
        String[] headerAndSequence = io.InOutFieldManager.findFastaHeaderMakeSequence(inputFileOrSequence);

        // Test if the DNA sequence contains invalid characters.
        InOutFieldManager testSequence = new InOutFieldManager();

        // Shows the end results of the tool.
        ArrayList<String> textResults;
        // Test if tool should run or just display an error in the results area.
        String textTestResult = testSequence.validateInputSequence(headerAndSequence[0]);
        if (textTestResult.equals(headerAndSequence[0])) {
            // Stores the results of the program.
            ArrayList<OrfProduct> translatedORFProducts;

            // Create an array in which the sequence, optionals and the header can be stored plus a
            // variable amount of frame shifts.
            // {DNASequence, frameShifts (1-6), optionalStart, optionalEnd, optionalMinimumLength, header}
            String[] sequenceInfoArray = new String[5 + requestedSequenceFrameShifts.length];

            // start,stop,minimum length inserted into sequenceInfoArray.
            sequenceInfoArray[sequenceInfoArray.length - 4] = io.InOutFieldManager.testInputIfFilledIn("optionalStart", request);
            sequenceInfoArray[sequenceInfoArray.length - 3] = io.InOutFieldManager.testInputIfFilledIn("optionalEnd", request);
            sequenceInfoArray[sequenceInfoArray.length - 2] = io.InOutFieldManager.testInputIfFilledIn("optionalMinimumLength", request);

            // Fill in frame shifts in the sequence info array.
            System.arraycopy(requestedSequenceFrameShifts, 0, sequenceInfoArray, 1,
                    requestedSequenceFrameShifts.length);
            // Fill in the sequence in the input for the program.
            sequenceInfoArray[0] = headerAndSequence[0];

            // Initialize program.
            RunningProgram runProgram = new RunningProgram();

            // In case there is a header give it to the program.
            if (headerAndSequence[1] != null) {
                sequenceInfoArray[sequenceInfoArray.length - 1] = headerAndSequence[1];
                translatedORFProducts = runProgram.initiateProgram(sequenceInfoArray);
            }
            // No header, most likely a sequence.
            else {
                translatedORFProducts = runProgram.initiateProgram(sequenceInfoArray);
            }
            textResults = io.InOutFieldManager.unpackSequenceProducts(translatedORFProducts);
        } else {
            textResults = new ArrayList<String>
                    (Arrays.asList("The input of the sequence does not contain valid characters:\n", textTestResult));
        }


        request.setAttribute("resultArea", textResults);
        RequestDispatcher view = request.getRequestDispatcher("upload.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Shows the web page on a get request.
        RequestDispatcher view = request.getRequestDispatcher("upload.jsp");
        view.forward(request, response);
    }

}
