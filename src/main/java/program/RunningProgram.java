package program;

import io.FrameConverter;
import models.DnaParser;
import models.OrfProduct;
import sequenceutilities.*;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Executes the inputs from the front and passes them to several other methods.
 */
public class RunningProgram {
    /**
     * Runs the steps.
     * @param args All arguments given from the website and reformed and reshaped into a String array.
     * @return The outcome of the sequence and its selected reading frames and optional values.
     */
    public  ArrayList<OrfProduct> initiateProgram(String[] args) {

        RunningProgram run = new RunningProgram();
        // Frame conversion to Int.
        FrameConverter readingFrameConverter = new FrameConverter();
        int [] validatedIntegerFrameArray = readingFrameConverter.
                stringFramesToIntegerFrames(Arrays.copyOfRange(args,1,args.length-4));

        // Processing the optional values.
        int [] optionalValues = optionalValueTesting(args[0].length(),args[args.length-4],args[args.length-3],args[args.length-2]);

        // Selects from the DNA sequence the start and stop and returns the selected region and the reverse complement of the selected region.
        String [] selectedRegionSequence = run.regionOfInterestMaker(args[0],optionalValues[0],optionalValues[1]);

        // In case a negative value is in the frame conversion the dna sequence has to be reversed

        // All tests on the data are done and return the results.
        return run.makeTranslation(validatedIntegerFrameArray,selectedRegionSequence[0],selectedRegionSequence[1],args[args.length-1],optionalValues[2]);
    }

    /**
     * Slices the DNA sequence based on the values given as start and stop.
     * @param givenSequence DNA sequence. (Without header.)
     * @param startRegion Start position of sliced sequence.
     * @param stopRegion Stop position of sliced sequence.
     * @return Sliced sequence and reversed sliced sequence.
     */
    private String [] regionOfInterestMaker(String givenSequence, int startRegion, int stopRegion){
        // Array with sliced sequence and the reversed complement of the sliced sequence.
        final String [] regionsOfInterest = new String[2];
        DnaSequenceModifiers sequenceModifier = new DnaSequenceModifiers();
        // Selects region.
        String dnaRegion = sequenceModifier.selectRegion(givenSequence,startRegion,stopRegion);
        // Normal.
        regionsOfInterest[0] = dnaRegion;
        // Reverse complement.
        regionsOfInterest[1] = sequenceModifier.reverseInputSequence(sequenceModifier.makeComplement(dnaRegion));
        return regionsOfInterest;
    }

    /**
     * Processes the sequence
     * @param readingFrames Reading frames in which the ORFs must be found.
     * @param dnaSequence DNA sequence in which the ORFs should be found.
     * @param reverseDNASequence The reverse complement of the DNA sequence.
     * @param originalHeader Header from the original fasta file.
     * @param minimumLength Length of which the ORFs at least should be.
     * @return All found ORFs in the given sequences and reading frames.
     */
    private ArrayList<OrfProduct> makeTranslation(int [] readingFrames,String dnaSequence, String reverseDNASequence, String originalHeader, int minimumLength){
        ArrayList<OrfProduct> allFoundORFs = new ArrayList<>();
        for(int readingFrame: readingFrames) {
            // Normal.
            if(readingFrame > 0) {
                DnaParser parseFrontReadingFrame = new DnaParser(readingFrame, dnaSequence,originalHeader,minimumLength);
                // Add the ORFs to the collection.
                allFoundORFs.addAll(parseFrontReadingFrame.getORFsInReadingFrame());
            // Reverse complement.
            } else{
                DnaParser parseBackReadingFrame = new DnaParser(readingFrame, reverseDNASequence,originalHeader,minimumLength);
                // Add the ORFs to the collection.
                allFoundORFs.addAll(parseBackReadingFrame.getORFsInReadingFrame());
            }
        }
        return allFoundORFs;
    }

    /**
     * Background check in case one of the optionals does not block the user from submitting.
     * Also catches an error if a fasta header is given but the minimum length,
     * start or stop value is larger than the input sequence.
     * @param sequenceLength Length of the DNA sequence.
     * @param start Optional start value, start slice point of DNA sequence.
     * @param stop Optional stop value , stop slice point of DNA sequence.
     * @param minimumLength Optional minimum length.
     * @return The correct optional values that can be further processed in an int array.
     */
    private int[] optionalValueTesting(int sequenceLength, String start, String stop, String minimumLength){
        // Stores optional will be converted to int values.
        int [] validOptionals = new int[3];

        // Nothing in the start.
        if (start.equals("") || start.equals(null)){
            validOptionals[0] = 0;
        }
        else{
            // Convert String value to int.
            validOptionals[0] = Integer.valueOf(start);
        }

        // Nothing in the stop.
        if (stop.equals("") || stop.equals(null)){
            validOptionals[1] = sequenceLength;
        }
        else{
            validOptionals[1] = Integer.valueOf(stop);
            // To large start compared to stop value.
            if (validOptionals[0] > validOptionals[1]){
                // Convert to zero.
                validOptionals[0] = 0;
                // Convert to maximum length.
                validOptionals[1] = sequenceLength;
            }
        }

        // Nothing in the minimum.
        if (minimumLength.equals("") || stop.equals(null)){
            validOptionals[2] = 3;
        }
        else{
            // To large minimum, not useful.
            if(sequenceLength < Integer.valueOf(minimumLength)){
                validOptionals[2] = 3;
            }
            // Fill in minimum length.
            validOptionals[2] = Integer.valueOf(minimumLength);
        }

        return validOptionals;
    }

}
