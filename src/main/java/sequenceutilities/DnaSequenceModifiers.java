package sequenceutilities;

import models.ComplementConverter;

import java.util.Arrays;

/**
 * Few utilities that modify DNA sequences.
 */
public class DnaSequenceModifiers {

    /**
     * Make DNA sequence complement.
     * @param dnaSequence Whole DNA sequence without header.
     * @return DNA complement.
     */
    public String makeComplement (String dnaSequence){
        String [] complementSequence = new String [dnaSequence.length()];
        // Split out and iterate to convert each character.
        String [] splitDnaSequence = dnaSequence.split("");
        ComplementConverter complementCaller = new ComplementConverter();
        for (int i = 0; i < dnaSequence.length(); i++){
            complementSequence[i] = complementCaller.convertDnaCharacter(splitDnaSequence[i]);
        }
        return String.join("",complementSequence);
    }

    /**
     * Reverses a DNA sequence.
     * @param inputSequence Whole DNA sequence without header.
     * @return Reverse the sequence.
     */
    public  String reverseInputSequence(String inputSequence){
        return new StringBuilder(inputSequence).reverse().toString();
    }

    /**
     * Selects a region from the DNA sequence.
     * @param inputSequence Whole DNA sequence without header.
     * @param optionalStart Start position given where the sequence should be sliced.
     * @param optionalStop Stop position given where the sequence should be sliced.
     * @return Sliced DNA sequence.
     */
    public String selectRegion(String inputSequence,int optionalStart, int optionalStop){
        return inputSequence.substring(optionalStart,optionalStop);
    }


}
